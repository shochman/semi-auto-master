export function setup(ctx) {
	// debug
	const debugLog = (...msg) => {
		//console.debug(`[${id} v4174]`, ...msg);
	};

	// script details
	const id = 'semi-auto-master';
	const title = 'SEMI Auto Master';

	// setting groups
	const SETTING_GENERAL = 'General';
	const SETTING_SKILLS = 'Per Skill Toggles';

	// variables
	const MAX_ATTEMPTS = 250;
	const LOG_INTERVAL = 1000;

	// utils
	const htmlID = (id) => id.replace(/[:_]/g, '-').toLowerCase();

	// Mastery Value Cache
	const masteryTokenXP = {};
	const masteryPoolXP = {}; // XP for generally 95% pool.
	game.masterySkills.forEach(skill => {
		if (!skill.hasMastery)
			return;

		masteryTokenXP[skill.id] = Math.floor((skill.baseMasteryPoolCap * skill.masteryToken.modifiers.masteryToken[0].value) / 100);
		masteryPoolXP[skill.id] = skill.baseMasteryPoolCap * (masteryCheckpoints[masteryCheckpoints.length - 1] / 100);
	});

	// Mastery Actions
	const spendTokens = (skill, qty) => game.bank.claimItemOnClick(skill.masteryToken, qty);
	const actionXPCost = (skill, action) => exp.level_to_xp(skill.getMasteryLevel(action) + 1) + 1 - skill.getMasteryXP(action);

	/**
	* Get the lowest mastery action, excluding masteries equal to or above the mastery level cap.
	*/
	const lowestMastery = (skill) => {
		let skillActions = skill.actions.allObjects;
		let masteryCap = skill.masteryLevelCap; // Normally 99

		let lowestAction;
		let lowestLevel = masteryCap;

		for (let n = 0; n < skillActions.length; n++) {
			let actionMastery = skill.getMasteryLevel(skillActions[n]);

			if (actionMastery >= masteryCap)
				continue;

			if (actionMastery < lowestLevel) {
				lowestAction = skillActions[n];
				lowestLevel = actionMastery;

				if (lowestLevel <= 1)
					break;
			}
		}

		return lowestAction;
	};

	/**
	* Check whether we should spend pool XP to upgrade a Mastery Action.
	This take into account bonus XP claimable from tokens if provided.
	*/
	const canSpendXP = (skill, action, bonusXP = 0) => {
		// Under final checkpoint XP value.
		if (skill.masteryPoolXP < masteryPoolXP[skill.id])
			return false;

		// Pool is Capped
		if (skill.masteryPoolXP === skill.masteryPoolCap)
			return true;

		const xpCost = actionXPCost(skill, action);

		// No enough XP to spend.
		if (skill.masteryPoolXP < xpCost)
			return false;

		// Spending would put under checkpoint, including bonus XP from tokens.
		if (skill.masteryPoolXP - xpCost + bonusXP < masteryPoolXP[skill.id])
			return false;

		return true;
	};

	/**
	* Handle a skills mastery upgrades.
	*/
	const handleSkill = (skill, useFastEscape = false) => {
		// No Mastery
		if (!skill.hasMastery)
			return;

		// No Mastery to Level
		let initialMasteryAction = lowestMastery(skill);
		if (!initialMasteryAction)
			return;

		// Get Settings		
		const autoMasterEnabled = ctx.settings.section(SETTING_GENERAL).get(`${id}-enable`);
		const autoMasterSkillEnabled = ctx.settings.section(SETTING_SKILLS).get(`skill-${htmlID(skill.id)}`);

		if(!autoMasterEnabled || !autoMasterSkillEnabled) {
			debugLog('autoMasterEnabled:', autoMasterEnabled, 'autoMasterSkillEnabled:', autoMasterSkillEnabled);
			return;
		}

		const allButOneIsEnabled = mod.api.SEMI.getSubmodSettings('SEMI All But One', 'all-but-one');
		const displayNotificationsSetting = game.settings.showMasteryCheckpointconfirmations;

		const tokenCount = (skill) => (game.bank.getQty(skill.masteryToken) - (allButOneIsEnabled ? 1 : 0));

		const spendableTokens = (skill) => {
			const tokenQty = tokenCount(skill);
			if (tokenQty > 0) {
				const xpPerToken = masteryTokenXP[skill.id];
				const xpRemaining = skill.masteryPoolCap - skill.masteryPoolXP;
				const tokensToFillPool = Math.floor(xpRemaining / xpPerToken);
				return Math.min(tokenQty, tokensToFillPool);
			}
			return 0;
		}

		// Use Tokens, check for final pool checkpoint.
		if (useFastEscape) {
			// Use Tokens
			const bankTokens = spendableTokens(skill);
			if (bankTokens > 0) {
				spendTokens(skill, bankTokens);
			}
			
			const bankTokenXP = masteryTokenXP[skill.id] * tokenCount(skill);
			// Can't afford initial mastery action.
			if(!canSpendXP(skill, initialMasteryAction, bankTokenXP)) {
				return;
			}
		}

		// Begin Mastery Handling
		const startTime = performance.now();
		debugLog(`[${skill.name}] Beginning mastery spending`);

		// Disable Checkpoint Notifications
		if (displayNotificationsSetting) {
			game.settings.toggleSetting('showMasteryCheckpointconfirmations')
		}

		// Blackhole NotificationQueue and Toastify
		const _notificationAdd = game.combat.notifications.add.bind(game.combat.notifications);
		game.combat.notifications.add = function(notification) {};
		const _toastifyShowToast = Toastify.prototype.showToast;
		Toastify.prototype.showToast = function() {}

		// Level Up multiple masteries.
		for (var n = 1; n <= MAX_ATTEMPTS; n++) {
			let tokenQty;
			let postSpendTokens = 0;
			let bestMastery = lowestMastery(skill);

			if (!bestMastery) {
				debugLog(`[${skill.name}] No more master actions to level up, finished in ${n} loops.`);
				break;
			}

			let canAfford = canSpendXP(skill, bestMastery);

			// Cannot afford, use tokens and check again.
			if (!canAfford) {
				tokenQty = spendableTokens(skill);
				if (tokenQty > 0) {
					spendTokens(skill, tokenQty);
					canAfford = canSpendXP(skill, bestMastery);
				}
			}

			// Can't afford but would have enough tokens to restore pool.
			if (!canAfford) {
				tokenQty = tokenCount(skill);
				if (tokenQty > 0) {
					let maxTokenXP = masteryTokenXP[skill.id] * tokenQty;
					let actionCost = actionXPCost(skill, bestMastery);

					if (skill.masteryPoolXP - actionCost + maxTokenXP > masteryPoolXP[skill.id]) {
						postSpendTokens = tokenQty;
						canAfford = true;
					}
				}
			}

			// Still can't afford, bail.
			if (!canAfford) {
				debugLog(`[${skill.name}] Not enough mastery XP / tokens to afford upgrade.`);
				break;
			}

			// Level Up
			skill.levelUpMasteryWithPoolXP(bestMastery, 1);

			// Refill Pool
			if (postSpendTokens > 0) {
				spendTokens(skill, spendableTokens(skill));
			}

			// Loop Logging
			if (n % LOG_INTERVAL === 0) {
				debugLog(`[${skill.name}] Loop ${n} / ${MAX_ATTEMPTS}`)
			}
		}

		// Restores Checkpoint Notifications
		if (displayNotificationsSetting) {
			game.settings.toggleSetting('showMasteryCheckpointconfirmations')
		}

		// Restore NotificationQueue and Toastify
		game.combat.notifications.add = _notificationAdd;
		Toastify.prototype.showToast = _toastifyShowToast;

		const endTime = performance.now();
		debugLog(`[${skill.name}] Mastery spending complete, took ${endTime - startTime}ms.`);
	};

	const handleAllSkills = () => {
		const startTime = performance.now();
		
		const displayNotificationsSetting = game.settings.showMasteryCheckpointconfirmations;

		// Disable Notifications if enabled.
		if (displayNotificationsSetting) {
			game.settings.toggleSetting('showMasteryCheckpointconfirmations')
		}

		game.masterySkills.forEach(skill => {
			handleSkill(skill);
		});

		// Restores Notifications
		if (displayNotificationsSetting) {
			game.settings.toggleSetting('showMasteryCheckpointconfirmations')
		}
		
		const endTime = performance.now();
		debugLog(`handleAllSkills took ${endTime - startTime}ms.`);
	};
	
	// settings
	ctx.settings.section(SETTING_GENERAL).add({
		'type': 'switch',
		'name': `${id}-enable`,
		'label': `Enable ${title}`,
		'default': true
	});
	
	ctx.settings.section(SETTING_SKILLS).add(
		game.masterySkills.filter(skill => skill.hasMastery).map(skill => {
			return {
				'type': 'switch',
				'name': `skill-${htmlID(skill.id)}`,
				'label': `Enable ${skill.name}`,
				'default': true
			}
		})
	);

	// game hooks
	ctx.onCharacterLoaded(ctx => {
		handleAllSkills();
	});

	ctx.onInterfaceReady(ctx => {
		ctx.patch(Skill, 'addXP').after(function(result, xpAmount, masteryAction) {
			handleSkill(this, true);
		});
	});
}
